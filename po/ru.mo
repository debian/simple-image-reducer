��    '      T  5   �      `  )   a     �  %   �     �     �     �     �     	       	   "  "   ,     O     S     r     �     �     �     �     �     �     �  	   �  	   �     �     �     �       .        6     C     K  !   e     �     �     �  `  �  #        ?  L  W  F   �	  3   �	  6   
  
   V
  ,   a
  5   �
  "   �
     �
          #  @   5     v  K   z  ?   �               *  !   .     P     j  7   o     �     �     �     �            h        �     �  /   �  F   �  ,   )  D   V     �  ^  �  <   �  (   <                       &   "   #          
       '   	                                                                    !          %                                           $       %(current)d of %(total)d images processed %d images to process (c) Copyright 2010 Konstantin Korikov 180° 90° clockwise 90° counter-clockwise According to EXIF data Add File... Add files... All Files Append "-%s" to the file base name BMP Cannot identify image file: %s Drag image files here File Fit to: GIF Image Files Input Files: JPEG Modify images in place No change No rotate Output files: Output format: PNG PPM Reduce and rotate images in three-four clicks. Remove files Rotate: Save to "%s" subdirectory Select a maximum width and height Select a rotation method Simple Image Reducer TIFF This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with this program; if not, see http://www.gnu.org/licenses/ Unable to open file for writing: %s Unable to open file: %s Project-Id-Version: Simple Image Reducer 1.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-12-24 17:38+0200
PO-Revision-Date: 2010-12-11 02:20+0200
Last-Translator: Konstantin Korikov <lostclus@gmail.com>
Language-Team: Russian
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Обработано %(current)d из %(total)d изображений %d изображений для обработки (c) Copyright 2010 Кориков Константин На 180° На 90° по часовой стрелке На 90°  против часовой стрелки Согласно данным EXIF Добавить файл... Добавить файлы... Все файлы Добавить "-%s" к базовому имени файла BMP Невозможно определить тип изображения: %s Перетащите сюда файлы изображений Файл Подогнать под: GIF Файлы изображений Входные файлы JPEG Изменить изображения на месте Не менять Не поворачивать Выходные файлы: Выходной формат: PNG PPM Уменьшает и поворачивает изображения в три-четыре клика. Удалить файлы Повернуть: Сохранить в подкаталог "%s" Выберите максимальную ширину и высоту Выберите метод вращения Простой преобразователь изображений TIFF Данная программа является свободным ПО. Вы вправе распространять и/или
модифицировать ее в рамках условий лицензии GNU General Public License
опубликованной Free Software Foundation версии 2 либо (на Ваш выбор)
более поздней версии.

Программа распространяеся в надежде на то, что она будет полезна, однако
НЕ ПРЕДОСТАВЛЯЕТСЯ НИКАКИХ ГАРАНТИЙ, в том числе ГАРАНТИИ ТОВАРНОГО
СОСТОЯНИЯ ПРИ ПРОДАЖЕ и ПРИГОДНОСТИ ДЛЯ ИСПОЛЬЗОВАНИЯ В КОНКРЕТНЫХ
ЦЕЛЯХ. Для получения более подробной информации ознакомьтесь с 
GNU General Public License.

Вместе с данной программой вы должны были получить экземпляр GNU General
Public License. Если нет, смотрите http://www.gnu.org/licenses/ Не открывается файл для записи: %s Не открывается файл: %s 