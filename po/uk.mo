��    '      T  5   �      `  )   a     �  %   �     �     �     �     �     	       	   "  "   ,     O     S     r     �     �     �     �     �     �     �  	   �  	   �     �     �     �       .        6     C     K  !   e     �     �     �  `  �  #        ?  �  W  >   6
  +   u
  4   �
  
   �
  8   �
  7     )   R     |     �     �  -   �     �  B   �  =   7     u     ~     �     �     �     �  3   �               -     H     g     k  d   o     �     �  /     D   4  .   y  :   �     �     �  D   �  3   .                       &   "   #          
       '   	                                                                    !          %                                           $       %(current)d of %(total)d images processed %d images to process (c) Copyright 2010 Konstantin Korikov 180° 90° clockwise 90° counter-clockwise According to EXIF data Add File... Add files... All Files Append "-%s" to the file base name BMP Cannot identify image file: %s Drag image files here File Fit to: GIF Image Files Input Files: JPEG Modify images in place No change No rotate Output files: Output format: PNG PPM Reduce and rotate images in three-four clicks. Remove files Rotate: Save to "%s" subdirectory Select a maximum width and height Select a rotation method Simple Image Reducer TIFF This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with this program; if not, see http://www.gnu.org/licenses/ Unable to open file for writing: %s Unable to open file: %s Project-Id-Version: Simple Image Reducer 1.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-12-24 17:38+0200
PO-Revision-Date: 2010-12-13 02:03+0300
Last-Translator: Konstantin Korikov <lostclus@gmail.com>
Language-Team: Ukrainian
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Virtaal 0.6.1
 %(current)d з %(total)d зображень оброблено %d зображень для обробки (c) Copyright 2010 Коріков Костянтин На 180° На 90° за годинниковою стрілкою 90° проти годинникової стрілки Відповідно до даних EXIF Додати файл... Додати файли... Всі файли Додати "-%s" до імені файлу BMP Не можу визначити файл зображення: %s Перетягніть сюди файли зображень Файл Підігнати під: GIF Файли зображень Вхідні файли: JPEG Змінити зображення на місці Без змін Без змін Вихідні файли: Вихідний формат: PNG PPM Зменшення і обертання зображень в три-чотири клацання. Вилучити файли Повернути: Зберегти в підкаталозі "%s" Виберіть максимальну ширину і висоту Виберіть метод обертання Простий перетворювач зображень TIFF Ця програма є вільним програмним забезпеченням. Ви маєте право
розповсюджувати її в рамах умови GNU General Public License,
опублікованій через Free Software Foundation, версія 2 або (на
Ваш вибір) будь-яка інша пізніша версія.

Програма поширюється в надії на те, що вона буде корисна, однак
не надає ЖОДНОЇ ГАРАНТІЇ, у тому числі ГАРАНТІЇ ТОВАРНОГО СТАНУ
ДЛЯ ПРОДАЖІ і ПРИДАТНОСТІ ДО ВИКОРИСТАННЯ В ПЕВНИХ ЦІЛЯХ. Для
отримання більш детальної інформації ознайомтеся з GNU General
Public License.

Ви повинні отримати копію GNU General Public License разом з цією
програмою. Якщо ні, то дивіться http://www.gnu.org/licenses/ Неможливо відкрити файл для запису: %s Не вдається відкрити файл: %s 